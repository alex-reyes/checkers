from collections import namedtuple
from enum import Enum


BoardPosition = namedtuple('BoardPosition', ['x', 'y'])


def position_from_str(s):
    xy = [int(v) for v in s.lstrip('(').rstrip(')').split(',')]
    return BoardPosition(*xy)


class CheckerColor(Enum):
    black = 1
    red = 2


class CheckerPiece(object):
    COLOR_CHAR_MAP = {
        CheckerColor.black: 'o',
        CheckerColor.red: 'x',
    }

    def __str__(self):
        return '%s,%s,%s' % (self.print_for_board, self.position.x, self.position.y)

    def __init__(self, color, position, forward):
        self.color = color
        self.forward = forward
        self.position = position
        self.kinged = False

    @property
    def char(self):
        return CheckerPiece.COLOR_CHAR_MAP[self.color]

    @property
    def print_for_board(self):
        if self.kinged:
            return 2 * self.char
        else:
            return self.char


class CheckerBoard(object):
    HEIGHT = WIDTH = 8
    SQUARE_PRINT_WIDTH = 3
    BLACK_INFO = {
        'starting_positions': [
            BoardPosition(*t) for t in
            [(0, 0), (2, 0), (4, 0), (6, 0), (1, 1), (3, 1), (5, 1), (7, 1), (0, 2), (2, 2), (4, 2), (6, 2)]],
        'forward': 1,
        'char': 'o'
    }
    RED_INFO = {
        'starting_positions': [
            BoardPosition(*t) for t in
            [(1, 5), (3, 5), (5, 5), (7, 5), (0, 6), (2, 6), (4, 6), (6, 6), (1, 7), (3, 7), (5, 7), (7, 7)]],
        'forward': -1,
        'char': 'x'
    }

    def __init__(self):
        self.active_pieces = {}
        for position in CheckerBoard.BLACK_INFO['starting_positions']:
            self.active_pieces[position] = CheckerPiece(CheckerColor.black, position, CheckerBoard.BLACK_INFO['forward'])
        for position in CheckerBoard.RED_INFO['starting_positions']:
            self.active_pieces[position] = CheckerPiece(CheckerColor.red, position, CheckerBoard.RED_INFO['forward'])
        self.game_over = False

    def print_board(self):
        horizontal_line = '+' + (('-' * CheckerBoard.SQUARE_PRINT_WIDTH + '+') * CheckerBoard.WIDTH)
        line_list = [horizontal_line]
        for y in range(CheckerBoard.HEIGHT):
            line = '|'
            for x in range(CheckerBoard.WIDTH):
                if (x, y) in self.active_pieces:
                    piece_printed = self.active_pieces[(x, y)].print_for_board
                    square_printed = ' ' + piece_printed + ' ' * (CheckerBoard.SQUARE_PRINT_WIDTH - len(piece_printed) - 1) + '|'
                else:
                    square_printed = ' ' * CheckerBoard.SQUARE_PRINT_WIDTH + '|'
                line += square_printed
            line_list.append(line)
            line_list.append(horizontal_line)
        print('\n'.join(reversed(line_list)))

    def position_in_bounds(self, position):
        return 0 <= position.x < CheckerBoard.WIDTH and 0 <= position.y < CheckerBoard.HEIGHT

    def get_moves_for_piece(self, piece):
        # regular movement
        movement_options = [
            position for position in
            [
                BoardPosition(piece.position.x - 1, piece.position.y + piece.forward),
                BoardPosition(piece.position.x + 1, piece.position.y + piece.forward),
            ]
            if self.position_in_bounds(position) and position not in self.active_pieces
        ]
        return movement_options

    def get_moves_for_piece_at_xy(self, x, y):
        position = BoardPosition(x, y)
        try:
            piece = self.active_pieces[position]
        except KeyError:
            print('no piece found at %s' % position)
            return None
        return self.get_moves_for_piece(piece)


def movement_procedure(board, player_color):
    while True:
        position_str = game_input('choose piece to move: ', help_text="enter position in format 'x, y' or enter c to cancel movement")

        if position_str == 'c':
            print('movement cancelled')
            return

        # specify position of piece
        try:
            position = position_from_str(position_str)
        except ValueError:
            print('invalid position specification (h for help)')
            continue

        # get piece for position
        try:
            piece = board.active_pieces[position]
        except KeyError:
            if not board.position_in_bounds(position):
                print('(%s, %s) is not a position on the board' % position)
            else:
                print('no piece found at (%s, %s)' % position)
            continue

        # get moves available for piece
        if piece.color != player_color:
            print('you cannot move piece %s (you are %s)' % (piece, player_color))
            continue
        moves_available = board.get_moves_for_piece(piece)
        if not moves_available:
            print('no moves available for piece %s' % piece)
        else:
            print('available moves for piece %s: %s' % (piece, moves_available))

        # choose move
        move_index = game_input('which move would you like to make? ', help_text="c to cancel")
        if move_index == 'c':
            print('movement cancelled')
            continue
        try:
            move = moves_available[int(move_index)]
        except (ValueError, IndexError):
            print('invalid movement')
            continue

        print('doing move %s' % str(move))


class PlayerQuit(Exception):
    pass


def game_input(prompt, help_text):
    s = input(prompt)
    while s in ('h', 'help', '?'):
        print(' '.join([help_text, '(q to quit, h to print help)']))
        s = input(prompt)
    if s == 'q':
        raise PlayerQuit()
    else:
        return s

if __name__ == '__main__':
    board = CheckerBoard()
    player_color = CheckerColor.black
    board.print_board()
    while not board.game_over:
        try:
            player_input = game_input('enter command: ', help_text='m to move')
            if player_input == 'm':
                movement_procedure(board, player_color)
            else:
                print('invalid command (h for help)')
                continue
        except PlayerQuit:
            board.game_over = True
            continue
